"""
Python client module for "apiservice" REST API service.
"""

import ast
import json
import requests

from common import MyGlobals


def _configList(configStr):
    try:
        if '*' in configStr:
            return lambda x: True
        idSet = set(int(c) for c in configStr.split(',')
                    if c.strip())

        def _check(cid):
            try:
                return int(cid) in idSet
            except:
                pass
            return False

        return _check
    except:
        pass
    return lambda x: False


_enabled = _configList(MyGlobals.apiservice_apiservice_enabled_companies)


class NotEnabledException(Exception):
    def __init__(self, companyId):
        super(NotEnabledException, self).__init__(
            'apiservice not enabled for company %s' % companyId)


class _Service:
    _PORT = 8080
    _BASE_URL = 'http://localhost:{}'.format(_PORT)

    def __callService(self, method, path, **kwargs):
        fullUrl = self._BASE_URL + self.resourcePath + path
        try:
            r = method(fullUrl, **kwargs)
            r.raise_for_status()
        except requests.exceptions.HTTPError, e:
            # Include server response in exception message
            try:
                msg = e.args[0] + ': ' + e.response.text.strip()
                e.args = (msg,)
            except:
                pass
            raise e
        return r

    def get(self, path):
        return self.__callService(requests.get, path)


class Company(_Service):
    def __init__(self, companyId):
        if _enabled(companyId) is False:
            raise NotEnabledException(companyId)

        self.resourcePath = '/company/{}'.format(companyId)

    def apiUsers(self, app):
        """Get emails of users configured for API scan."""
        path = '/apiusers/{}'.format(app)
        r = self.get(path).json()
        for user in r['users']:
            yield user

        while r.get('next_page'):
            r = self.get('{}?page={}'.format(path, r['next_page'])).json()
            for user in r['users']:
                yield user

    def dlps(self, app):
        """Get DLP patterns configured for API scan."""
        rj = self.get('/dlps/{}'.format(app)).json()

        # Serialize JSON strings 
        for pattern in rj.get('patterns', {}).values():
            if 'code' in pattern:
                try:
                    pattern['code'] = json.loads(pattern['code'])
                except:
                    pass
            if 'validators' in pattern:
                validators = pattern['validators']
                pattern['validators'] = json.loads(validators) if validators else []
            for match in ('include', 'exclude'):
                if pattern.get(match):
                    for key in ('keywords', 'regex'):
                        data = pattern[match].get(key)
                        pattern[match][key] = json.loads(data) if data else []
        return rj

    def apiPolicy(self, email):
        """Get compiled API policies for given user."""
        rj = self.get('/user/{}/apipolicy'.format(email)).json()

        for app in rj['rules'].values():
            for policy in app:
                condition = policy['rule']['condition']
                policy['rule']['condition'] = ast.literal_eval(condition) if condition else {}

        return rj

    def domains(self):
        """Get domains for company."""
        return self.get('/domains').json()['domains']

    def admins(self):
        """Get admin users of company."""
        path = '/admins'
        r = self.get(path).json()
        for admin in r['admins']:
            yield admin

        while r.get('next_page'):
            r = self.get('{}?page={}'.format(path, r['next_page'])).json()
            for admin in r['admins']:
                yield admin

    def groups(self, email):
        """Get group IDs for user."""
        return self.get('/user/{}/groups'.format(email)).json()['groups']

    def adminEmailEnabled(self):
        """Get admin_email_enabled setting for company."""
        return bool(self.get('/adminemailenabled').json()['enabled'])

    def apiEnabled(self, app):
        return bool(self.get('/apienabled/{}'.format(app)).json()['enabled'])